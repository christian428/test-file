#Test file for chapter exmaples. 
puts "hello world"

# ///////////////////// #

#Method that returns a personalized greeting.

def say_goodnight(name)
    result = 'Good night, ' + name 
    return result
end

puts say_goodnight('Mary-Ellen')

# ///////////////////// #

puts 1 + 2 #addition 
puts "cow" + "boy" #concatenation

# ///////// # 

#Method that returns a personalized greeting.


def say_goodnight(name)
    result = 'Good morning, ' + name 
    return result
end

puts say_goodnight('John-boy')

# ///////////////////// #

#Interpolation in double-quoted strings. In the string, the sequence #{expression} is replaced by the value of expression. 
#We could use this to rewrite our previous method.

def say_goodnight (name)
    "Goodnight, #{name.capitalize}"
end
puts say_goodnight('pa')

# /////////// #

# Arrays and Hashes

a = [1 , 'cat', 3.14]
puts a[0] #Prints 1
puts a[2] = nil #Special reserved value for an absent value
puts a # Prints new array 
a[2] = 'meow' #Changing value of index position 2 to be meow within array 
puts a #Printing new array 

###############

# blocks and iterators 

def wrap &b
    print "Santa says: " 
    3.times(&b)
    print "\n"
end
wrap { print "Ho! " }

###############

#Arrays and Hashes key value pairs 
#Left is the key and right is the corresponding value 

inst_section = { 
:cello => 'string', 
:clarinet => 'woodwind',
:drum => 'percussion',
:oboe => 'woodwind',
:trumpet => 'brass',
:violin => 'string'
}

puts inst_section[:oboe]